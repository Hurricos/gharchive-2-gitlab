CPUS=24

dates:
	seq 1 50000 | while read value; do date -d "$$value hours ago" +%Y-%m-%d-%k; done | tr -d ' ' > $@

JSON_GZS=$(addprefix gharchive/, $(addsuffix .json.gz,$(shell cat dates)))

gharchive/%.json.gz:
	wget -O "$@" https://data.gharchive.org/$*.json.gz

all: $(JSON_GZS)

out/ytdl_basic_issues.tsv: jq/ytdlBasicIssueGetter.jq
	find gharchive/ -type f -print0 | \
		parallel -0 -L1 -P$(CPUS) 'zcat {} | jq -r -f $<' | \
		awk '!x[$$0]++;' > $@;
