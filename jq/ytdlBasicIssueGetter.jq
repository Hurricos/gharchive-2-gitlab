def object2array(stream):
    foreach stream as $x (null;
                          #.[0]=false;
                          if . == null then $x | [true, keys_unsorted] else .[0]=false end;
                          (if .[0] then .[1] else empty end),
                          .[1] as $keys | $x | [getpath( $keys[] | [.]) ] );

object2array
(

 select(.repo.id==1039520) |
 select(.type=="IssueCommentEvent") |
 {
  action: .payload.action,
  url: .payload.issue.url,
  user: .payload.comment.user.login,
  state: .payload.issue.state,
  date: .payload.comment.created_at,
  body: .payload.comment.body
  }
 ) | map(.) | @tsv
